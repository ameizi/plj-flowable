-- 运行相关的历史数据
TRUNCATE TABLE act_hi_actinst;
TRUNCATE TABLE act_hi_attachment;
TRUNCATE TABLE act_hi_comment;
TRUNCATE TABLE act_hi_detail;
TRUNCATE TABLE act_hi_entitylink;
TRUNCATE TABLE act_hi_identitylink;
TRUNCATE TABLE act_hi_procinst;
TRUNCATE TABLE act_hi_taskinst;
TRUNCATE TABLE act_hi_tsk_log;
TRUNCATE TABLE act_hi_varinst;
-- 运行相关数据
TRUNCATE TABLE act_ru_actinst;
TRUNCATE TABLE act_ru_deadletter_job;
TRUNCATE TABLE act_ru_entitylink;
TRUNCATE TABLE act_ru_event_subscr;
TRUNCATE TABLE act_ru_history_job;
TRUNCATE TABLE act_ru_identitylink;
TRUNCATE TABLE act_ru_job;
TRUNCATE TABLE act_ru_suspended_job;
TRUNCATE TABLE act_ru_timer_job;
TRUNCATE TABLE act_ru_variable;
SET foreign_key_checks = 0;
TRUNCATE TABLE act_ru_execution;
TRUNCATE TABLE act_ru_task;
SET foreign_key_checks = 1;