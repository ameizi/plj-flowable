# 

>    
### 说明文档 


http://localhost:8888/


* 此处可以不用修改
![修改保存模型弹框](../doc/img/修改保存模型弹框.png)
>
    static/editor-app/popups/save-model.html
    100行左右，修改model


# *此处必须修改

![保存模型后跳转到自己页面](../doc/img/保存模型后跳转到自己页面.png)
>
    static/editor-app/configuration/toolbar-default-actions.js
    360行 ，跳转
    
    
    
参考： http://www.sparkplatform.cn/#/model    



将modeler不放到static，而是其他目录下：
![路径修改](../doc/img/Modeler路径修改.png)
>
    1.第一步：
    @Configuration
    public class WebMvcConfig implements WebMvcConfigurer {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/modeler/**").addResourceLocations("classpath:/modeler/");
        }
    }
    2.修改配置： scripts/app-cfg.js
![配置修改](../doc/img/Modeler配置修改.png)    

    var FLOWABLE = FLOWABLE || {};
    var pathname = window.location.pathname.replace(/^(\/[^\/]*)(\/.*)?$/, '$1').replace(/\/$/, '');
    if (pathname.indexOf("/modeler") > -1) {
    	pathname = pathname.replace("/modeler","");
    }
    FLOWABLE.CONFIG = {
    	'onPremise' : true,
    	'contextRoot' : pathname,
    	'webContextRoot' : pathname+"/modeler",
    	'datesLocalization' : false
    };
    
   3.测试访问：
   http://localhost:8888/modeler/index.html?modelId=f2deaf25-f34e-11ea-8a87-405bd8b55521#/editor/c34c19d2-f3c6-11ea-bb73-405bd8b55521
   
   
   
* 动态指点下一个节点的执行人：
> 
    流程图中 
        分配用户->固定值中填：${userId}       
    代码
        Map variables = Maps.newHashMap();
        variables.put("userId",this.getLoginUser().getId());
        map.put("variables",variables);
        //发起流程
        Result result = flowClient.startByKey(map);
   
   
   
   
   
   