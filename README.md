### 介绍
***
Flowable工作流

### 软件架构
***
1.  Flowable6.5.0
2.  Springboot2.3.3
3.  MybatisPlus
4.  Mysql5.7

测试通过： 
>>>
    1.与业务绑定；
    2.全局监听（执行节点前，后，结束 监听），实现消息通知；
    3.发送邮件；
    4.排他网关，并行网关，多实例（会签），子流程；
    5.回退上一步，回退到历史中的任意节点
    6.添加批注信息；
    7.自定义用户及用户组；
    8.自定义流程图样式（颜色，字体大小等）。


-------------------------------------------------


### 2021年11月20日 重新开发V2.0,在线访问地址：http://pangliujie.top
    用户名：admin 密码：123456  （V2.0还在开发中，代码：[https://gitee.com/liujie1346/PLJ](https://gitee.com/liujie1346/PLJ)）
>>>
    MybatisPlus3.4.3.4 
    Flowable6.5.0
    Vue+Iview
    Mysql
    Thumbnailator 
    纯真IP库
>>>
    增加表单设计器，多实例加签减签

![输入图片说明](doc/image.png)

![输入图片说明](doc/img/1image.png)

![输入图片说明](doc/img/2image.png)

![输入图片说明](doc/img/image.png)