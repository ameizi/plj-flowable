package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @description
* @author        庞留杰
* @date          2020/8/24 13:48
* @version       V1.0
**/
public interface MyService extends IService<Map<String,Object>>  {

    List<Map<String,Object>> findUsersAndRoleIds(String filter);

    List<Map<String,Object>> findRoles(String filter);

    String test();

}
