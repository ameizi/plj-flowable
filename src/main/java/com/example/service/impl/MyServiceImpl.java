package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mapper.MyMapper;
import com.example.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
* @description
* @author        庞留杰
* @date          2020/8/24 13:43
* @version       V1.0
**/
@Service
public class MyServiceImpl extends ServiceImpl<MyMapper, Map<String,Object>> implements MyService {

    @Autowired
    MyMapper myMapper;

    @Override
    public List<Map<String, Object>> findUsersAndRoleIds(String filter) {
        return myMapper.findUsersAndRoleIds(filter);
    }

    @Override
    public List<Map<String, Object>> findRoles(String filter) {
        return myMapper.findRoles(filter);
    }

    @Override
    public String test() {
        return "test.....";
    }
}
