package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/index")
public class IndexController {

    @GetMapping("/list")
    public ModelAndView list(ModelAndView model) {
        model.setViewName("my-list-modeler.html");
        return model;
    }

    @GetMapping("/check")
    public ModelAndView check(ModelAndView model) {
        model.setViewName("my-check-modeler.html");
        return model;
    }
}
