package com.example.mapper;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.SQL;

/**
 * @description
 * @author 庞留杰
 * @date 2020/8/24 13:42
 * @version V1.0
 **/
public class MyProvider {

    public String findUsersAndRoleIds(String filter) {
        return new SQL() {{
            SELECT("su.id AS user_id, " +
                    " su.user_code, " +
                    " su.user_name, " +
                    " su.real_name, " +
                    " su.password, " +
                    " su.email, " +
                    " (SELECT GROUP_CONCAT(sur.role_id) FROM sys_user_role sur WHERE sur.user_id=su.id) AS role_ids");
            FROM(" sys_user su ");
            WHERE(" su.is_delete=0 ");
            if (!StringUtils.isEmpty(filter)) {
                WHERE("su.real_name like '%" + filter + "%'");
            }
        }}.toString();
    }

    public String findRoles(String filter) {
        return new SQL() {{
            SELECT(" sr.* ");
            FROM("  sys_role sr ");
            WHERE(" sr.is_delete=0");
            if (!StringUtils.isEmpty(filter)) {
                WHERE(" sr.role_name like '%" + filter + "%'");
            }
        }}.toString();
    }

}
