package com.example.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

/**
* @description
* @author        庞留杰
* @date          2020/8/24 13:42
* @version       V1.0
**/
public interface MyMapper extends BaseMapper<Map<String,Object>> {


    @SelectProvider(method = "findUsersAndRoleIds",type = MyProvider.class)
    List<Map<String,Object>> findUsersAndRoleIds(String filter);

    @SelectProvider(method = "findRoles",type =  MyProvider.class)
    List<Map<String,Object>> findRoles(String filter);

}
