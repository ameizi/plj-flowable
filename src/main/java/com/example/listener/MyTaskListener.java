package com.example.listener;

import lombok.extern.slf4j.Slf4j;
import org.flowable.task.service.delegate.BaseTaskListener;
import org.flowable.task.service.delegate.DelegateTask;
import org.flowable.task.service.delegate.TaskListener;
import org.springframework.stereotype.Component;

/**
 *@Description 任务监听器
 *@Author 庞留杰
 *@Date2020/9/11 8:39
 *@Version V1.0
 **/
@Slf4j
@Component(value = "myListener")
public class MyTaskListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        log.info(">>>>>>>>>任务监听器,参数：{}>>>>>>>>>",delegateTask);
        String eventName = delegateTask.getEventName();
        switch (eventName) {
            case BaseTaskListener.EVENTNAME_CREATE:
                log.info("当前监听创建事件:create");
                break;
            case BaseTaskListener.EVENTNAME_ASSIGNMENT:
                log.info("当前监听指派事件:assignment");
                break;
            case BaseTaskListener.EVENTNAME_COMPLETE:
                log.info("当前监听完成事件:complete");
                delegateTask.setVariable("XXX","添加一个参数测试一下！");
                break;
            case BaseTaskListener.EVENTNAME_DELETE:
                log.info("当前监听销毁事件:delete");
                break;
            default:
                break;
        }
    }
}
