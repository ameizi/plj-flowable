package com.example.listener;

import com.example.service.MyService;
import com.example.utils.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.flowable.task.service.delegate.BaseTaskListener;
import org.flowable.task.service.delegate.DelegateTask;
import org.flowable.task.service.delegate.TaskListener;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 *@Description 动态添加审批人
 *@Author 庞留杰
 *@Date2020/9/11 8:39
 *@Version V1.0
 **/
@Slf4j
@Component
public class DynamicListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {

        MyService myService = SpringUtil.getObject(MyService.class );
        SpringUtil.showClass();

        log.info(">>>>>>>>>任务监听器,参数：{}>>>>>>>>>",delegateTask);
        String eventName = delegateTask.getEventName();
        switch (eventName) {
            case BaseTaskListener.EVENTNAME_CREATE:

                System.out.println("测试myService："+ myService);
                System.out.println("测试test："+ myService.test());

                log.info("当前监听创建事件:create");
                System.out.println("delegateTask==="+delegateTask.getProcessInstanceId());//流程实例ID
                delegateTask.setVariable("XXX","添加一个参数测试一下！");
                delegateTask.addCandidateUsers(Arrays.asList("1","2"));
                break;
            case BaseTaskListener.EVENTNAME_ASSIGNMENT:
                log.info("当前监听指派事件:assignment");
                break;
            case BaseTaskListener.EVENTNAME_COMPLETE:
                log.info("当前监听完成事件:complete");
                delegateTask.setVariable("XXX","添加一个参数测试一下！");
                break;
            case BaseTaskListener.EVENTNAME_DELETE:
                log.info("当前监听销毁事件:delete");
                break;
            default:
                break;
        }
    }
}
