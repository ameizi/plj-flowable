package com.example.listener;

import lombok.extern.slf4j.Slf4j;
import org.flowable.task.service.delegate.BaseTaskListener;
import org.flowable.task.service.delegate.DelegateTask;
import org.flowable.task.service.delegate.TaskListener;
import org.springframework.stereotype.Component;

/**
 *@Description 知会监听
 *@Author 庞留杰
 *@Date2020/9/11 8:39
 *@Version V1.0
 **/
@Slf4j
@Component
public class NotifyListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        log.info(">>>>>>>>>任务监听器,参数：{}>>>>>>>>>",delegateTask);
        String eventName = delegateTask.getEventName();
        switch (eventName) {
            case BaseTaskListener.EVENTNAME_CREATE:
                System.out.println("发送一条消息1");
                break;
            case BaseTaskListener.EVENTNAME_ASSIGNMENT:
                System.out.println("发送一条消息2");
                break;
            case BaseTaskListener.EVENTNAME_COMPLETE:
                System.out.println("发送一条消息3");
                break;
            case BaseTaskListener.EVENTNAME_DELETE:
                System.out.println("发送一条消息4");
                break;
            default:
                break;
        }
    }
}
