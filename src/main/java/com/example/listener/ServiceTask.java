package com.example.listener;

import org.flowable.common.engine.api.delegate.Expression;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 *@Description
 *@Author 庞留杰
 *@Date2020/12/1 16:14
 *@Version V1.0
 **/
public class ServiceTask implements JavaDelegate {

    //流程变量
    private Expression text1;

    //重写委托的提交方法
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("serviceTask已经执行已经执行！");
        String value1 = (String) text1.getValue(execution);
        System.out.println(value1);
        execution.setVariable("var1", new StringBuffer(value1).reverse().toString());
    }


}
