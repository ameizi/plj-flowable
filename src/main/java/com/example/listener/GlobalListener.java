package com.example.listener;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.BaseExecutionListener;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;

/**
 * 执行监听器 （sequenceFlow的监听器）
 * @Package: com.spark.platform.flowable.biz.listener
 * @ClassName: GlobalListener
 */
@Slf4j
@Component(value = "globalListener")
public class GlobalListener implements ExecutionListener {

    @Override
    public void notify(DelegateExecution delegateExecution) {
        String defId = delegateExecution.getProcessDefinitionId();
        String instanceId = delegateExecution.getProcessInstanceId();
        String eventName = delegateExecution.getEventName();
        log.info(">>>>>>>>>>>>>>>>流程定义ID:{}   流程实例ID:{}>>>>>>>>>>>>>>>>", defId, instanceId);
        delegateExecution.getCurrentActivityId();
        log.debug(">>>>>>>>>>>>>>>>监听器事件名称：{}>>>>>>>>>>>>>>>>", eventName);
        switch (eventName){
            case BaseExecutionListener.EVENTNAME_START:
                System.out.println(">>>>>>>全局监听执行了start>>>>");
                log.info(">>>>>>>>>>>>>>>>{}事件执行了start>>>>>>>>>>>>>>>>", eventName);break;
            case BaseExecutionListener.EVENTNAME_END:
                System.out.println(">>>>>>>全局监听执行了end>>>>");
                log.info(">>>>>>>>>>>>>>>>{}事件执行了end>>>>>>>>>>>>>>>>", eventName);break;
            case BaseExecutionListener.EVENTNAME_TAKE:
                System.out.println(">>>>>>>全局监听执行了take>>>>>");
                // 执行后可以添加参数
                // delegateExecution.setVariable("SYSTEM_JUDGE_SUBMIT_VALUE","pass");
                log.info(">>>>>>>>>>>>>>>>{}事件执行了take>>>>>>>>>>>>>>>>", eventName);break;
            default:
                break;
        }
    }
}
