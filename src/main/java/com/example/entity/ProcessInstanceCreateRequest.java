package com.example.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 流程发起
 */
@Data
public class ProcessInstanceCreateRequest {
   //流程定义ID
    private String processDefinitionId;
    //流程KEY
    private String processDefinitionKey;
    //业务ID
    private String businessKey;
    //租户ID
    private String tenantId;
    //业务类型
    private String businessType;
   //业务名称
    private String businessName;
    //流程参数
    private Map<String, Object> variables = new HashMap<>();

    public ProcessInstanceCreateRequest(){

    }

    public ProcessInstanceCreateRequest(String processDefinitionKey, String businessKey, String businessType, String businessName, Map<String, Object> var){
        setProcessDefinitionKey(processDefinitionKey);
        setBusinessKey(businessKey);
        setBusinessType(businessType);
        setBusinessName(businessName);
        variables.putAll(var);
        variables.put(businessType,businessType);
        variables.put(businessName,businessName);
    }
}
