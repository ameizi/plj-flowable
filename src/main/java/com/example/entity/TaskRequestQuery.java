package com.example.entity;

import lombok.Data;

import java.util.List;

/**任务查询query
 */
@Data
public class TaskRequestQuery {
   //任务ID
    private String taskId;
   //用户id
    private String userId;
    //用户groupIds
    private List<String> groupIds;
   //当前节点name
    private String name;
   //流程实例key
    private String taskDefinitionKey;
   //业务主键
    private String businessKey;
   //业务类型
    private String businessType;
   //业务名称
    private String businessName;
    //流程实例ID
    private String processInstanceId;
    /**
     * 页码
     */
    private long size;
    /**
     * 当前页
     */
    private long current;
}
