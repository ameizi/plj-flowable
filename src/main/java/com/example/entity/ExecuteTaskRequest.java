package com.example.entity;

import lombok.*;

import java.util.Map;

/**
 * task任务执行
 */
@EqualsAndHashCode(callSuper = false)
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class ExecuteTaskRequest {
    //执行任务类型
    private String action;
    //受让人
    private String assignee;
    //流程参数存储范围
    private Boolean localScope = false;
    //参数
    private Map<String, Object> variables;
}
