package com.example.common;

import com.example.common.exception.BusinessRuntimeException;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *@Description 参数校验工具类
 *@Author 庞留杰
 *@Date2020/8/27 8:45
 *@Version V1.0
 **/
public class VerifyUtil {

    public static void notEmpty(String msg,Object param) {
        String errMsg = String.format("参数[%s]不能为空",msg);
        if(param == null){
            throw new BusinessRuntimeException(errMsg);
        }
        if (param instanceof Map && 0 == ((Map) param).size()) {
            throw new BusinessRuntimeException(errMsg);
        } else if (param instanceof Collection && 0 == ((Collection) param).size()) {
            throw new BusinessRuntimeException(errMsg);
        } else if (param instanceof String && 0 == ((String) param).length()) {
            throw new BusinessRuntimeException(errMsg);
        } else if (param instanceof Number && 0 == ((Number) param).doubleValue()) {
            throw new BusinessRuntimeException(errMsg);
        }
    }

    public static void isEmail(String msg,String param) {
        String errMsg = String.format("邮箱[%s]格式不正确",msg);
        String emailPattern = "^([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)*@([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)+[\\.][A-Za-z]{2,3}([\\.][A-Za-z]{2})?$";
        Pattern p = Pattern.compile(emailPattern);
        Matcher m = p.matcher(param);
        if(!m.matches()){
            throw new BusinessRuntimeException(errMsg);
        }
    }

    /**
     *
     * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
     * 此方法中前三位格式有：
     * 13+任意数
     * 145,147,149
     * 15+除4的任意数(不要写^4，这样的话字母也会被认为是正确的)
     * 166
     * 17+3,5,6,7,8
     * 18+任意数
     * 198,199
     *
     * */
    public static void isPhone(String msg,String param) {
        String errMsg = String.format("电话[%s]格式不正确",msg);
        // ^ 匹配输入字符串开始的位置
        // \d 匹配一个或多个数字，其中 \ 要转义，所以是 \\d
        // $ 匹配输入字符串结尾的位置
        String regExp = "^((13[0-9])|(14[5,7,9])|(15[0-3,5-9])|(166)|(17[3,5,6,7,8])" +
                "|(18[0-9])|(19[8,9]))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(param);
        if(!m.matches()){
            throw new BusinessRuntimeException(errMsg);
        }
    }

    /*
     * 判断是否为整数
     */
    public static void isInteger(String msg,String param) {
        String errMsg = String.format("参数[%s]格式不正确",msg);
        Pattern p = Pattern.compile("^[-\\+]?[\\d]*$");
        Matcher m = p.matcher(param);
        if(!m.matches()){
            throw new BusinessRuntimeException(errMsg);
        }
    }


    /**
     * 验证参数是否为数值
     */
    public  static void isNumber(String msg,String param) {
        String errMsg = String.format("参数[%s]格式不正确",msg);
        Pattern p = Pattern.compile("-?[0-9]+(\\.[0-9]+)?");
        Matcher m = p.matcher(param);
        if(!m.matches()){
            throw new BusinessRuntimeException(errMsg);
        }
    }

    //金额验证
    public static void isMoney(String msg,String param){
        String errMsg = String.format("参数[%s]格式不正确",msg);
        Pattern pattern=Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){2})?$"); // 判断小数点后2位的数字的正则表达式
        Matcher m=pattern.matcher(param);
        if(!m.matches()){
            throw new BusinessRuntimeException(errMsg);
        }
    }

}
