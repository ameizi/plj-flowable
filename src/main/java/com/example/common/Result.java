package com.example.common;


import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Exrickx
 * 前后端交互数据标准
 */
@Data
public class Result<T> implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 成功标志
     */
    private boolean success;

    /**
     * 消息
     */
    private String message;

    /**
     * 返回代码
     */
    private String code;

    /**
     * 时间戳
     */
    private long timestamp = System.currentTimeMillis();
//    private long timestamp = SystemClock.now();

    /**
     * 结果对象
     */
    private T result;

    public String toJsonString() {
        return JSONObject.toJSONString(this);
    }
}
