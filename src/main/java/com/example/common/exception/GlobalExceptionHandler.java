package com.example.common.exception;

import com.example.common.Result;
import com.example.common.ResultEnums;
import com.example.common.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
* @description   全局异常处理
* @author        庞留杰
* @date          2020/7/23 16:50
* @version       V1.0
**/
@ControllerAdvice
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class )
    protected Result exp(HttpServletRequest request, Exception e) {
        return ResultUtil.error(ResultEnums.INTERNAL_SERVER_ERROR.getCode(), ResultEnums.INTERNAL_SERVER_ERROR.getReasonPhraseCN(), e.getMessage());
    }

    @ExceptionHandler(value = BusinessRuntimeException.class)
    protected Result exp(HttpServletRequest request, BusinessRuntimeException e) {
        return ResultUtil.error(ResultEnums.FAIL.getCode(), e.getMsg());
    }
}
