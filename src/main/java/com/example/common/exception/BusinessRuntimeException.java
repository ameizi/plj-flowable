package com.example.common.exception;

import lombok.Data;

/**
* @description   自定义运行时异常
* @author        庞留杰
* @date          2020/7/23 16:52
* @version       V1.0
**/
@Data
public class BusinessRuntimeException extends RuntimeException {

    private String msg;

    public BusinessRuntimeException(String msg){
        super(msg);
        this.msg = msg;
    }
}
