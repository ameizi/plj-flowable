/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.flowable.ui.common.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


/**
 * @description  此处代码复制于flowable-ui-common-6.5.0.jar,重写flowable的方法
 *                1. flowable在开发模式(dev)下,会自动连接本地数据库,重写此类并注释掉里面的方法
 *                可以使我们在开发模式(dev)下连接自己需要的数据库
 *                2. !!!  此类的包名不能变，需要和源码重名
 * @author        庞留杰
 * @date          2020/9/19 21:18
 * @version       V1.0
 */
/**
 * Development @Profile specific datasource override
 *
 * @author Yvo Swillens
 */
@Configuration(proxyBeanMethods = false)
@Profile({"dev"})
public class DevelopmentConfiguration {

    /*********************** !!! 重写Flowable的类,注释掉一下内容 *************************/
//    protected static final String DATASOURCE_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
//    protected static final String DATASOURCE_URL = "jdbc:mysql://127.0.0.1:3306/flowable?characterEncoding=UTF-8";
//    protected static final String DATASOURCE_USERNAME = "flowable";
//    protected static final String DATASOURCE_PASSWORD = "flowable";

//    @Bean
//    @Primary
//    public DataSource developmentDataSource() {
//        return DataSourceBuilder
//                .create()
//                .driverClassName(DATASOURCE_DRIVER_CLASS_NAME)
//                .url(DATASOURCE_URL)
//                .username(DATASOURCE_USERNAME)
//                .password(DATASOURCE_PASSWORD)
//                .build();
//    }

}